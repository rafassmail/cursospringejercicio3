package com.rafasoriazu.controller;

import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.rafasoriazu.model.Contact;
import com.rafasoriazu.validators.ContactValidator;

@Controller
public class HolaMundoController {
	
    @Autowired
    protected ContactValidator contactValidator;

 
    private List<Contact> contactList= new ArrayList<Contact>();

	
    @RequestMapping("/hola")
    @ResponseBody
        public String helloWorld(Model model) {
            model.addAttribute("message", "Hola Mundo!");
           return "Hola Mundo";
        }
    
    @RequestMapping("/holaConJsp")
	public ModelAndView holaConJsp() {
        Hashtable<String, String>model= new Hashtable();
        model.put("message", "Hola Mundo!");
	    return new ModelAndView("respuesta", model);
	}
    
    @RequestMapping(value="/holaConJsp", method=RequestMethod.GET)
	public ModelAndView printMensaje(@RequestParam("mensaje") String message) {
        Hashtable<String, String>model= new Hashtable();
        model.put("message", message);
	    return new ModelAndView("respuesta", model);
	}
	
    @RequestMapping(value="/holaConJsp/{mensaje}", method=RequestMethod.GET)
   	public ModelAndView printMensajeFromURL(@PathVariable(value="mensaje") String message) {
           Hashtable<String, String>model= new Hashtable();
           model.put("message", message);
   	    return new ModelAndView("respuesta", model);
   	}
    
    @ModelAttribute("contacts")
    public List<Contact> getAllContacts() {
     /*Contact contact= new Contact();
     contact.setEmail("aaaa@aaaa.es");
     contact.setName("Luis");
     contact.setId(new Long(1));
     this.contactList.add(contact);
     contact= new Contact();
     contact.setEmail("eee@eee.es");
     contact.setName("Pedro");
     contact.setId(new Long(2));
     this.contactList.add(contact);*/
     return this.contactList;
    }
   
   @RequestMapping(value = "/contacts", method = RequestMethod.GET)
   public String listContacts(@ModelAttribute("contactAttribute")Contact contact){
      return "contactList";        
   }

   @RequestMapping(value = "/contacts", method = RequestMethod.POST)
   public String addContact(@Valid @ModelAttribute("contactAttribute")Contact contact, BindingResult result){
	   if (result.hasErrors()) return "contactList";
       else{
	       Contact copy= new Contact();
	       copy.setEmail(contact.getEmail());
	       copy.setId(contact.getId());
	       copy.setName(contact.getName());
	       contactList.add(copy);
	       contact.setEmail("");
	       contact.setId(null);
	       contact.setName("");
       }
      return "contactList";        
   }
    
}
