<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
 <%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
 <%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
 <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title><spring:message code="contact.title" text="Texto por defecto" /></title>

<style type="text/css">
.error{background-color: red;
}
</style>


</head>
<body>
<spring:message var="email_label" code="contact.email" />
<h1><spring:message code="contact.title" text="Texto por defecto" /></h1>
<h3><spring:message code="contact.language" /> : <a href="?language=en">English</a>|<a href="?language=es">Espa�ol</a></h3>

 <form:form modelAttribute="contactAttribute" method="POST" action="contacts">
 <ul>
 
 <li><form:label path="id">Id:</form:label><form:input path="id"/> <form:errors path="id"  cssClass="error"/></li>
 <li><form:label path="email">${email_label}:</form:label><form:input path="email"/><form:errors path="email"  cssClass="error"/></li>
 <li><form:label path="name"><spring:message code="contact.name" /></form:label><form:input path="name"/><form:errors path="name"  cssClass="error"/></li>
 <li><input type="submit" value="Save" /></li>
 </ul>
 </form:form>
<h2><spring:message code="contact.resultados" /></h2>
	<c:if test="${not empty contacts}">
		<ul>
			<c:forEach var="contact" items="${contacts}">
				<li>${contact.email}</li>
				<li>${contact.name}</li>
				<li>${contact.id}</li>
			</c:forEach>
		</ul>
	</c:if>
</body>
</html>